//
//  CoreDatamanager.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 1/10/22.
//

import CoreData
import UIKit.UIApplication

typealias CDUserResponse = [User]
class CoreDataManager {
    static let shared = CoreDataManager()
    var context: NSManagedObjectContext?

    init() {
        self.context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
    }

    func getLocalUsers() -> CDUserResponse? {
        if let context = context {
            do {
                let fetch = User.fetchRequest()
                let listUsers = try context.fetch(fetch)
                return listUsers
            } catch {
                print(error.localizedDescription)
                return nil
            }
        } else {
            return nil
        }
    }

    func createUser(users: UsersResponse, completion: @escaping(Bool) -> Void) {
        if let context = context {
            users.forEach { item in
                let user = User(context: context)
                user.companyBs = item.company.bs
                user.companyName = item.company.name
                user.companyCatchPhrase = item.company.catchPhrase
                user.website = item.website
                user.phone = item.phone
                user.street = item.address.street
                user.suite = item.address.suite
                user.city = item.address.city
                user.zipcode = item.address.zipcode
                user.lat = item.address.geo.lat
                user.lng = item.address.geo.lng
                user.email = item.email
                user.username = item.username
                user.name = item.name
                user.id = Int32(item.id)
                saveContext(context: context)
            }
            completion(true)
        } else {
            completion(false)
        }
    }

    func saveContext(context: NSManagedObjectContext) {
        do {
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }
}
