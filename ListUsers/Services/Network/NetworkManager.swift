//
//  NetworkManager.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 30/09/22.
//

import Foundation
import Alamofire
class NetworkManager {
    static let shared = NetworkManager()
    let manager = Alamofire.Session()

    func request<T: Codable>(resource: endpointResource, completion: @escaping(Bool, T?) -> Void) {
        let url = EnvironmentManager.shared.baseUrl + resource.endPoint
        let dataRequest = AF.request(url, method: resource.httpMethod)
        dataRequest.responseDecodable(of: T.self) { response in
            switch response.result {
            case .success(let response):
                completion(true, response)
            case .failure(_):
                completion(false, nil)
            }
        }
    }
}
