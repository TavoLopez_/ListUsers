//
//  EnvironmentManager.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 30/09/22.
//

import Foundation
import Alamofire
public enum BaseUrl: String {
    case environmentPath = "https://jsonplaceholder.typicode.com"
}
public typealias endpointResource = (httpMethod: HTTPMethod, endPoint: String)
class EnvironmentManager {
    static let shared = EnvironmentManager()
    let baseUrl: String
    
    init() {
        self.baseUrl = BaseUrl.environmentPath.rawValue
    }
}
