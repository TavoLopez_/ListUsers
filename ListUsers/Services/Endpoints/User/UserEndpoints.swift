//
//  UserEndpoints.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 30/09/22.
//

import Foundation

enum UserEndpoints {
    case users
    case userPosts(Int32)

    var resource: endpointResource {
        switch self {
        case .users:
            return (.get, "/users")
        case .userPosts(let id):
            return (.get, "/posts?userId=\(id)")
        }
    }
}


