//
//  PostEndpoints.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 1/10/22.
//

import Foundation

enum PostEndpoints {
    case posts

    var resource: endpointResource {
        switch self {
        case .posts:
            return (.get, "/posts")
        }
    }
}
