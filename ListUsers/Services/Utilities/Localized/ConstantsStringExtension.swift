//
//  ConstantsStringExtension.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 1/10/22.
//

import Foundation
extension String {
    // MARK: Cell identifiers
    static let userCellIdentifier = "UserCollectionViewCell"
    static let postCellIdentifier = "PostsCollectionViewCell"
    static let emptyStateNameXib = "EmptyStateView"
    // MARK: Named colors
    static let colorPrimary = "colorPrimary"
    static let shadowColor = "shadowColor"
    // MARK: - Texts
    static let displayPosts = "MOSTRAR PUBLICACIONES"
    static let emptyState = "List is empty"
    // MARK: - Icons
    static let phoneFill = "phone.fill"
    static let envelopeFill = "envelope.fill"
    
}
