//
//  ParamsEncoder.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 30/09/22.
//

import Foundation
class ParamsEncoder {
    private let jsonEncoder = JSONEncoder()
    func encode<T>(_ value: T) throws -> Any where T: Codable {
        let jsonData = try jsonEncoder.encode(value)
        return try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
    }
}
