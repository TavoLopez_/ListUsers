//
//  ParamsDecoder.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 30/09/22.
//

import Foundation
class ParamsDecoder {
    private let jsonDecoder = JSONDecoder()

    func decode<T>(_ type: T.Type, from json: Any) throws -> T where T: Codable {
        let jsonData = try JSONSerialization.data(withJSONObject: json, options: [])
        return try jsonDecoder.decode(type, from: jsonData)
    }
}
