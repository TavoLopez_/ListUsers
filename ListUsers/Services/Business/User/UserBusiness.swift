//
//  UserBusiness.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 30/09/22.
//

import Foundation
public class UserBusiness {

    static let shared = UserBusiness()
    let networkManager = NetworkManager.shared

    func getUsers(completion: @escaping (Bool, UsersResponse?) -> Void) {
        let resource = UserEndpoints.users.resource
        networkManager.request(resource: resource, completion: completion)
    }
    
    func getUserPosts(for userId: Int32, completion: @escaping(Bool, PostsResponse?) -> Void) {
        let resource = UserEndpoints.userPosts(userId).resource
        networkManager.request(resource: resource, completion: completion)
    }
}
