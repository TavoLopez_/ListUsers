//
//  PostBusiness.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 30/09/22.
//

import Foundation
public class PostBusiness {

    static let shared = PostBusiness()
    let networkManager = NetworkManager.shared

    func getPosts(completion: @escaping(Bool, PostsResponse?) -> Void) {
        let resource = PostEndpoints.posts.resource
        networkManager.request(resource: resource, completion: completion)
    }
}
