//
//  PostsCollectionViewCell.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 1/10/22.
//

import UIKit

class PostsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    static let identifier: String = .postCellIdentifier

    static func nib() -> UINib {
        let bundle = Bundle(for: PostsCollectionViewCell.self)
        return UINib(nibName: .postCellIdentifier, bundle: bundle)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setUpView(post: PostModel?) {
        guard let post = post else { return }
        self.titleLabel.text = post.title
        self.bodyLabel.text = post.body
    }

}
