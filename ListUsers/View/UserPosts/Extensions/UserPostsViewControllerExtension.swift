//
//  UserPostsViewControllerExtension.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 1/10/22.
//

import UIKit

extension UserPostsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let viewModel = viewModel {
            let numberOfItems = viewModel.userPosts?.count
            return numberOfItems ?? 0
        } else {
            return 0
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let viewModel = viewModel {
            let cellIdentifier = PostsCollectionViewCell.identifier
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,
                                                             for: indexPath) as? PostsCollectionViewCell {
                cell.setUpView(post: viewModel.userPosts?[indexPath.row])
                return cell
            }
            return UICollectionViewCell()
        }
        return UICollectionViewCell()
    }
}

//extension UserPostsViewController: UICollectionViewDelegateFlowLayout {
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 360, height: 104)
//    }
//}

extension UserPostsViewController: UserPostsViewModelDelegate {
    func onFinishGetUserPosts() {
        userPostsCollectionView.reloadData()
    }
}
