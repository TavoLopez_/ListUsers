//
//  UserPostsViewModelProtocol.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 1/10/22.
//

import Foundation

// MARK: -  Protocol for HomeViewModel
protocol UserPostsViewModelProtocol {
    var delegate: UserPostsViewModelDelegate? { get set }
    func getUserPosts()
}

// MARK: - Delegate for HomeViewController
protocol UserPostsViewModelDelegate {
    func onFinishGetUserPosts()
}
