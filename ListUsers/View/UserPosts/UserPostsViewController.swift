//
//  UserPostsViewController.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 1/10/22.
//

import UIKit

class UserPostsViewController: UIViewController {

    @IBOutlet weak var userPostsCollectionView: UICollectionView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneImage: UIImageView!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var mailImage: UIImageView!
    @IBOutlet weak var mailLabel: UILabel!
    weak var viewModel: UserPostsViewModel?
    var user: User?
    override func viewDidLoad() {
        super.viewDidLoad()
        initViewModel()
        registerCells()
        setUpUI()
    }

    // configuration of view
    private func setUpUI() {
        setUpUserInfo()
        setDelegates()
        getPosts()
    }

    func setUpUserInfo() {
        guard let user = user else { return }
        self.nameLabel.text = user.name
        self.phoneLabel.text = user.phone
        self.mailLabel.text = user.email
    }

    // ViewModel initialization
    private func initViewModel() {
        viewModel = UserPostsViewModel.shared
        viewModel?.user = user
        viewModel?.delegate = self
    }

    // MARK: - The cell is registered for the collectionView
    private func registerCells() {
        userPostsCollectionView.register(PostsCollectionViewCell.nib(), forCellWithReuseIdentifier: PostsCollectionViewCell.identifier)
    }

    // Declaration of delegates
    private func setDelegates() {
        userPostsCollectionView.dataSource = self
    }

    // service call to get users to display in view
    private func getPosts() {
        guard let viewModel = viewModel else { return }
        viewModel.getUserPosts()
    }
}
