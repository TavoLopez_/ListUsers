//
//  HomeViewControllerExtension.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 30/09/22.
//

import UIKit

// MARK: - Delegate of searchBar in HomeViewController
extension HomeViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let viewModel = viewModel else { return }
        if !searchText.isEmpty {
            viewModel.filterUsers(text: searchText)
        } else {
            usersCollectionView.reloadData()
        }
    }
}

// MARK: - Datasource of collectionView in HomeViewController
extension HomeViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let viewModel = viewModel {
            let numberOfItems = isFiltering ? viewModel.filteredUsers?.count : viewModel.listUsers?.count
            return numberOfItems ?? 0
        } else {
            return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let viewModel = viewModel {
            let cellIdentifier = UserCollectionViewCell.identifier
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,
                                                             for: indexPath) as? UserCollectionViewCell {
                if isFiltering {
                    cell.setUpCell(with: viewModel.filteredUsers?[indexPath.row])
                } else {
                    cell.setUpCell(with: viewModel.listUsers?[indexPath.row])
                }
                cell.delegate = self
                return cell
            }
            return UICollectionViewCell()
        }
        return UICollectionViewCell()
    }
}

// MARK: - delegate of HomeViewController
extension HomeViewController: HomeViewControllerDelegate {
    func onFinishGetUsers() {
        showEmptyStateView(show: false)
        DispatchQueue.main.async {
            self.usersCollectionView.reloadData()
        }
    }

    // MARK: -
    func onFinishFiltering() {
        guard let viewModel = viewModel else { return }
        let showEmptyState = ((viewModel.filteredUsers?.count ?? 0) > 0)
        // Show empty State View
        showEmptyStateView(show: !showEmptyState)
        DispatchQueue.main.async {
            self.usersCollectionView.reloadData()
        }
    }
}

extension HomeViewController: UserCollectionCellDelegate {
    func onTabButton(user: User) {
        let bundle = Bundle(for: HomeViewController.self)
        let controller = UserPostsViewController(nibName: "UserPostsViewController", bundle: bundle)
        controller.user = user
        navigationController?.pushViewController(controller, animated: true)
    }
}
