//
//  UserCollectionViewCell.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 1/10/22.
//

import UIKit

class UserCollectionViewCell: UICollectionViewCell {

    // cell identifier
    static let identifier: String = .userCellIdentifier
    static func nib() -> UINib {
        let bundle = Bundle(for: UserCollectionViewCell.self)
        return UINib(nibName: .userCellIdentifier, bundle: bundle)
    }
    // Outlets
    @IBOutlet weak var nameLabelView: UILabel! {
        didSet {
            nameLabelView.textColor = .colorPrimary
        }
    }
    @IBOutlet weak var phoneLabelView: UILabel!
    @IBOutlet weak var emailLabelView: UILabel!
    @IBOutlet weak var iconPhoneView: UIImageView! {
        didSet {
            setUpImage(with: .phoneFill, for: iconPhoneView)
        }
    }
    @IBOutlet weak var iconEnvelopeView: UIImageView! {
        didSet {
            setUpImage(with: .envelopeFill, for: iconEnvelopeView)
        }
    }

    //MARK: Class attributes
    var delegate: UserCollectionCellDelegate?
    var userModel: User?
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    // MARK: - Set Data of views
    func setUpCell(with model: User?) {
        guard let model = model else { return }
        self.userModel = model
        self.nameLabelView.text = model.name
        self.phoneLabelView.text = model.phone
        self.emailLabelView.text = model.email
    }

    // MARK: - Configure set System Images
    private func setUpImage(with nameIcon: String, for imageView: UIImageView) {
        if let icon = UIImage(systemName: nameIcon) {
            imageView.image = icon
            imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = .colorPrimary
        }
    }

    // MARK: Actions
    @IBAction func showPostsButtonAction(_ sender: Any) {
        guard let userModel = userModel else { return }
        delegate?.onTabButton(user: userModel)
    }

}
