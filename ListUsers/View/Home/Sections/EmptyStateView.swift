//
//  EmptyStateView.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 1/10/22.
//

import UIKit
class EmptyStateView: CustomShowView {
    // MARK: - Outlets
    @IBOutlet weak var descriptionLabel: UILabel!
    override var nameXIB: String {.emptyStateNameXib}
    // MARK: - set text and color to label descrition
    func setUpView() {
        self.descriptionLabel.text = .emptyState
        self.descriptionLabel.textColor = .colorPrimary
    }
}
