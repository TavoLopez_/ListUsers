//
//  HomeViewModelProtocol.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 1/10/22.
//

import Foundation
// MARK: -  Protocol for HomeViewModel
protocol HomeViewModelProtocol {
    var delegate: HomeViewControllerDelegate? { get set }
    func getLocalUsers()
    func getUsersBusiness()
    func filterUsers(text: String)
}

protocol UserCollectionCellDelegate {
    func onTabButton(user: User)
}
// MARK: - Delegate for HomeViewController
protocol HomeViewControllerDelegate {
    func onFinishGetUsers()
    func onFinishFiltering()
}
