//
//  ViewController.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 29/09/22.
//

import UIKit

class HomeViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var searchBarController: UISearchBar!
    @IBOutlet weak var usersCollectionView: UICollectionView!
    @IBOutlet weak var emptyStateView: EmptyStateView!

    // MARK: - Class Atributes
    weak var viewModel: HomeViewModel?
    var isSearchBarEmpty: Bool {
        return searchBarController.text?.isEmpty ?? true
    }
    var isFiltering: Bool {
        return !isSearchBarEmpty
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        initViewModel()
        registerCells()
        setUpUI()
    }

    // configuration of view
    private func setUpUI() {
        emptyStateView.setUpView()
        setDelegates()
        getUsers()
    }

    // MARK: - show or hide Empty State View
    func showEmptyStateView(show: Bool) {
        emptyStateView.isHidden = !show
    }

    // ViewModel initialization
    private func initViewModel() {
        viewModel = HomeViewModel.shared
        viewModel?.delegate = self
    }

    // MARK: - The cell is registered for the collectionView
    private func registerCells() {
        usersCollectionView.register(UserCollectionViewCell.nib(),
                                     forCellWithReuseIdentifier: UserCollectionViewCell.identifier)
        
    }

    // Declaration of delegates
    private func setDelegates() {
        searchBarController.delegate = self
        usersCollectionView.dataSource = self
    }

    // service call to get users to display in view
    func getUsers() {
        guard let viewModel = viewModel else { return }
        viewModel.getLocalUsers()
    }
}

