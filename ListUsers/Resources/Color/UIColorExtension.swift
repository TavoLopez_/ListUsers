//
//  UIColorExtension.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 1/10/22.
//


import UIKit.UIColor
public extension UIColor {

    // MARK: This vars contains UIColor
    static var colorPrimary: UIColor {
        return .getColorPrimary()
    }
    static var shadowColor: UIColor {
        return .getShadowColor()
    }
    // MARK: This func return UIColor primary
    private static func getColorPrimary() -> UIColor {
        return UIColor(named: .colorPrimary) ?? UIColor.init()
    }
    private static func getShadowColor() -> UIColor {
        return UIColor(named: .shadowColor) ?? UIColor.init()
    }
}
