//
//  UserPostsViewModel.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 1/10/22.
//

import Foundation
class UserPostsViewModel: UserPostsViewModelProtocol {

    // MARK: - Class atributes
    static let shared = UserPostsViewModel()
    var business: UserBusiness? = UserBusiness.shared
    var delegate: UserPostsViewModelDelegate?
    var userPosts: PostsResponse?
    var isLoading: Bool = false
    var user: User?

    init(delegate: UserPostsViewModelDelegate? = nil) {
        self.delegate = delegate
    }

    // deinitializer
    deinit {
        business = nil
        delegate = nil
        userPosts = nil
    }

    func getUserPosts() {
        isLoading = true
        business?.getUserPosts(for: user?.id ?? 0, completion: { (success: Bool, postsResponse: PostsResponse?) in
            self.userPosts = success ? postsResponse : nil
            self.delegate?.onFinishGetUserPosts()
            self.isLoading = false
        })
    }
}
