//
//  HomeViewModel.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 30/09/22.
//

import UIKit
import CoreData
class HomeViewModel: HomeViewModelProtocol {

    // MARK: - Class atributes
    static let shared = HomeViewModel()
    var coreDataManager = CoreDataManager.shared
    var business: UserBusiness? = UserBusiness.shared
    var businessPosts: PostBusiness? = PostBusiness.shared
    var delegate: HomeViewControllerDelegate?
    var listUsers: CDUserResponse?
    var filteredUsers: CDUserResponse?
    var isLoading: Bool = false

    init(delegate: HomeViewControllerDelegate? = nil) {
        self.delegate = delegate
    }

    // deinitializer
    deinit {
        business = nil
        delegate = nil
    }

    func getLocalUsers() {
        listUsers = coreDataManager.getLocalUsers()
        if (listUsers?.count ?? 0) > 0 {
            delegate?.onFinishGetUsers()
        } else {
            getUsersBusiness()
        }
    }

    // service call to get users
    func getUsersBusiness() {
        isLoading = true
        business?.getUsers(completion: { (success: Bool, usersResponse: UsersResponse?) in
            if let users = usersResponse {
                self.coreDataManager.createUser(users: users) { success in
                    self.getLocalUsers()
                }
            }
            self.isLoading = false
        })
    }

    func filterUsers(text: String) {
        filteredUsers = listUsers?.filter {
            $0.name?.lowercased().contains(text.lowercased()) ?? false
        }
        self.delegate?.onFinishFiltering()
    }
}
