//
//  UserModel.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 30/09/22.
//

import Foundation
// MARK: - UserModel
struct UserModel: Codable {
    let id: Int
    let name, username, email: String
    let address: Address
    let phone, website: String
    let company: Company
}

// MARK: - Address
struct Address: Codable {
    let street, suite, city, zipcode: String
    let geo: Coordinate
}

// MARK: - Geo
struct Coordinate: Codable {
    let lat, lng: String
}

// MARK: - Company
struct Company: Codable {
    let name, catchPhrase, bs: String
}

typealias UsersResponse = [UserModel]
