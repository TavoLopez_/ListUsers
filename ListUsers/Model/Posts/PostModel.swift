//
//  PostModel.swift
//  ListUsers
//
//  Created by Gustavo Ospina on 30/09/22.
//

import Foundation

// MARK: - PostModel
struct PostModel: Codable {
    let userId, id: Int
    let title, body: String
}

typealias PostsResponse = [PostModel]
