//
//  HomeViewControllerTests.swift
//  ListUsersTests
//
//  Created by Gustavo Ospina on 1/10/22.
//

import XCTest
@testable import ListUsers

final class HomeViewControllerTests: XCTestCase {

    private var sut: HomeViewController!
    private var mockHomeViewModel: MockHomeViewModel?
    private var context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext

    override func setUp() {
        super.setUp()
        let storyBoard =  UIStoryboard(name: "Main", bundle: nil)
        guard let viewController = storyBoard.instantiateViewController(identifier: "MainView") as? HomeViewController else {
            XCTFail("Could not instantiate viewController as HomeViewController")
            return
        }
        sut = viewController
        sut.loadViewIfNeeded()
        mockHomeViewModel = MockHomeViewModel(delegate: sut)
        mockHomeViewModel?.listUser = getUsers()
    }

    func getUsers() -> CDUserResponse? {
        if let context = context {
            let user = User(context: context)
            user.companyBs = ""
            user.companyName = ""
            user.companyCatchPhrase = ""
            user.website = ""
            user.phone = ""
            user.street = ""
            user.suite = ""
            user.city = ""
            user.zipcode = ""
            user.lat = ""
            user.lng = ""
            user.email = ""
            user.username = ""
            user.name = ""
            user.id = 1
            return [user, user, user, user, user]
        }
        return nil
    }

    func test_getLocalUsers() {
        sut.viewModel?.getLocalUsers()
        XCTAssertTrue(sut.emptyStateView.isHidden)
    }
    
    func test_getUserBusiness() {
        mockHomeViewModel?.listUser = nil
        mockHomeViewModel?.usersResponse = getUsers()
        sut.viewModel?.getUsersBusiness()
        XCTAssertNotNil(sut.viewModel?.listUsers)
    }

    func test_viewModelisNilOn_getUsers() {
        sut.viewModel = nil
        sut.getUsers()
        XCTAssertNil(sut.viewModel)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
