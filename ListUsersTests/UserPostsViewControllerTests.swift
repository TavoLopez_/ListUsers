//
//  UserPostsViewControllerTests.swift
//  ListUsersTests
//
//  Created by Gustavo Ospina on 1/10/22.
//

import XCTest
@testable import ListUsers
final class UserPostsViewControllerTests: XCTestCase {

    private var sut: UserPostsViewController!
    private var mockUserPostsViewModel: MockUserPostsViewModel?
    private var context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext

    override func setUp() {
        super.setUp()
        let viewController = UserPostsViewController(nibName: "UserPostsViewController", bundle: nil)
        sut = viewController
        sut.loadViewIfNeeded()
        mockUserPostsViewModel = MockUserPostsViewModel(delegate: sut)
        mockUserPostsViewModel?.userPostList = getPosts()
    }

    func getPosts() -> PostsResponse? {
            let post = PostModel(userId: 1, id: 1, title: "", body: "")
            return [post, post]
    }
    
    func test_setUpUserInfo() {
        sut.setUpUserInfo()
        sut.user = getUser()
        XCTAssertFalse(sut.nameLabel.text != "Label")
        XCTAssertFalse(sut.phoneLabel.text != "Label")
        XCTAssertFalse(sut.mailLabel.text != "Label")
    }

    func test_setUpUserInfoWhenUserIsNil() {
        sut.setUpUserInfo()
        sut.user = nil
        XCTAssertNil(sut.user)
        XCTAssertFalse(sut.phoneLabel.text != "Label")
        XCTAssertFalse(sut.mailLabel.text != "Label")
    }

    func getUser() -> User? {
        if let context = context {
            let user = User(context: context)
            user.companyBs = ""
            user.companyName = ""
            user.companyCatchPhrase = ""
            user.website = ""
            user.phone = ""
            user.street = ""
            user.suite = ""
            user.city = ""
            user.zipcode = ""
            user.lat = ""
            user.lng = ""
            user.email = ""
            user.username = ""
            user.name = ""
            user.id = 1
            return user
        }
        return nil
    }
}
