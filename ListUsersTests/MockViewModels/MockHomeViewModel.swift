//
//  MockHomeViewModel.swift
//  ListUsersTests
//
//  Created by Gustavo Ospina on 1/10/22.
//

import Foundation
@testable import ListUsers

class MockHomeViewModel: HomeViewModel {
    var error: String?
    var listUser: CDUserResponse?
    var filteredUser: CDUserResponse?
    var usersResponse: CDUserResponse?

    override init(delegate: HomeViewControllerDelegate?) {
        super.init(delegate: delegate)
    }

    override func getLocalUsers() {
        if (listUser?.count ?? 0) > 0 {
            delegate?.onFinishGetUsers()
        } else {
            getUsersBusiness()
        }
    }

    override func getUsersBusiness() {
        if let _ = usersResponse {
            super.listUsers = usersResponse
        } else {
            error = "response api is nil"
        }
    }

    override func filterUsers(text: String) {
        filteredUser = listUser?.filter {
            $0.name?.lowercased().contains(text.lowercased()) ?? false
        }
        delegate?.onFinishFiltering()
    }
}
