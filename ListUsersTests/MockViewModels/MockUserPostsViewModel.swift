//
//  MockUserPostsViewModell.swift
//  ListUsersTests
//
//  Created by Gustavo Ospina on 1/10/22.
//

import Foundation
@testable import ListUsers

class MockUserPostsViewModel: UserPostsViewModel {
    var error: String?
    var userPostList: PostsResponse?
    var userModel: User?
    
    override init(delegate: UserPostsViewModelDelegate?) {
        super.init(delegate: delegate)
    }

    override func getUserPosts() {
        if let _ = userPostList {
            super.userPosts = userPostList
        } else {
            error = "response of get posts is nil"
        }
    }
}
